import { Country, Shipment, User } from "../models/shipment.model";

export class StorageUtil {

    //Country sessionStorage operations
    public static saveCountries(countries: Country[]): void {
        sessionStorage.setItem("Countries", JSON.stringify(countries));
    }
    public static readCountries(): Country[] | undefined {
        const value = sessionStorage.getItem("Countries")
        if(value){
            return JSON.parse(value) as Country[];
        }
        return undefined;
    }
    public static updateCountries(country: Country): void {
        const value = sessionStorage.getItem("Countries");
        if(value){
            const parsedValue = JSON.parse(value) as Country[];
            parsedValue.push(country);
            sessionStorage.setItem("Countries", JSON.stringify(parsedValue));
        }
        else{
            sessionStorage.setItem("Countries", JSON.stringify([country]));
        }
    }

    //User sessionStorage operations
    public static async saveUser(user: User): Promise<void> {
        console.log("Saving user ...")
        return Promise.resolve().then(() => {sessionStorage.setItem("User", JSON.stringify(user))});
    }
    public static readUser(): User | undefined {
        const value = sessionStorage.getItem("User")
        if(value){
            return JSON.parse(value);
        }
        return undefined;
    }

    //Shipment sessionStorage operations
    public static async saveShipments(shipments: Shipment[]): Promise<void> {
        return Promise.resolve().then(() => {sessionStorage.setItem("Shipments", JSON.stringify(shipments))});
    }
    public static readShipments(): Shipment[] | undefined {
        const value = sessionStorage.getItem("Shipments")
        if(value){
            return JSON.parse(value) as Shipment[];
        }
        return undefined;
    }
    public static updateShipments(shipment: Shipment): void {
        const value = sessionStorage.getItem("Shipments");
        if(value){
            const parsedValue = JSON.parse(value) as Shipment[];
            parsedValue.push(shipment);
            sessionStorage.setItem("Shipments", JSON.stringify(parsedValue));
        }
        else{
            console.log("Couldn't find shipments to update")
        }
    }
}