import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { RoleGuard } from './guards/role.guard';
import { AccountPage } from './pages/account/account.page';
import { AfterLoginPage } from './pages/after-login/after-login.page';
import { HomePage } from './pages/home/home.page';
import { SettingsPage } from './pages/settings/settings.page';
import { ShipmentsPage } from './pages/shipments/shipments.page';

const routes: Routes = [
  {
    path: "home",
    component: HomePage
  },
  {
    path: "",
    redirectTo: "/home",
    pathMatch: "full"
  },
  { path : "shipments",
    component: ShipmentsPage,
   canActivate: [ AuthGuard ]
  },
  { path : "account",
  component: AccountPage,
  canActivate: [ AuthGuard ]

  },
  { path : "afterlogin",
  component: AfterLoginPage,
  canActivate: [ AuthGuard ]

  },
  { path : "settings",
  component: SettingsPage,
  canActivate: [ RoleGuard ],
  data: {
    role: "admin"
  }

  }

];
@NgModule({
  imports: [RouterModule.forRoot(routes)], // Import a module
  exports: [RouterModule] // Expose module and its features

})
export class AppRoutingModule {}

