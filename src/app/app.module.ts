import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HeaderComponent } from './components/header/header.component';
import { SignInButtonComponent } from './components/sign-in-button/sign-in-button.component';
import { HomePage } from './pages/home/home.page';
import { NewShipmentButtonComponent } from './components/new-shipment-button/new-shipment-button.component';
import { SettingsPage } from './pages/settings/settings.page';
import { AccountPage } from './pages/account/account.page';
import { ShipmentsPage } from './pages/shipments/shipments.page';
import { FormsModule } from '@angular/forms';
import { UpdateButtonComponent } from './components/update-button/update-button.component';
import { ModalComponent } from './components/modal/modal.component';
import { CountrySettingsComponent } from './components/country-settings/country-settings.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RefreshTokenHttpInterceptor } from './interceptors/referesh-token-http.interceptor';
import { HttpAuthInterceptor } from './interceptors/auth-http.interceptor';
import { ShipmentItemComponent } from './components/shipment-item/shipment-item.component';
import { ShipmentModalComponent } from './components/shipment-modal/shipment-modal.component';
import { AfterLoginPage } from './pages/after-login/after-login.page';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HeaderComponent,
    SignInButtonComponent,
    HomePage,
    NewShipmentButtonComponent,
    SettingsPage,
    AccountPage,
    ShipmentsPage,
    UpdateButtonComponent,
    ModalComponent,
    CountrySettingsComponent,
    ShipmentItemComponent,
    ShipmentModalComponent,
    AfterLoginPage
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RefreshTokenHttpInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpAuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
