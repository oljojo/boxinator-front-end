import { Component, Input, OnInit } from '@angular/core';
import { User } from 'src/app/models/shipment.model';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-update-button',
  templateUrl: './update-button.component.html',
  styleUrls: ['./update-button.component.css']
})
export class UpdateButtonComponent implements OnInit {

  constructor(private readonly apiService: ApiService) { }

  @Input() user?: User;

  ngOnInit(): void {
  }
  updateUser(){
    console.log(this.user)
    this.apiService.putUser(this.user as User)
  }
}
