import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Shipment } from 'src/app/models/shipment.model';
import { ModalService } from 'src/app/services/modal.service';
import keycloak from 'src/keycloak';

@Component({
  selector: 'app-shipment-item',
  templateUrl: './shipment-item.component.html',
  styleUrls: ['./shipment-item.component.css']
})
export class ShipmentItemComponent implements OnInit {

  isHidden: boolean = true;
  
  get role(): boolean {
    return Boolean(keycloak.hasRealmRole("admin"));
  }

  @Input() shipment?: Shipment

  constructor(public router: Router, public modalService: ModalService) { }

  ngOnInit(): void {
  }

  toggleItem(): void {
    this.isHidden = !this.isHidden;
  }
}
