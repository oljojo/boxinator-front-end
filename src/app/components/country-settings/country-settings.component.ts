import { Component, OnInit } from '@angular/core';
import { Country } from 'src/app/models/country.model';
import { StorageUtil } from 'src/app/utils/storage.util';
import { ApiService } from 'src/app/services/api.service';


@Component({
  selector: 'app-country-settings',
  templateUrl: './country-settings.component.html',
  styleUrls: ['./country-settings.component.css']
})
export class CountrySettingsComponent implements OnInit {


  constructor(private apiService: ApiService) { }
  countries = <Country[]>[];
  editRate: number = 0;
  currentCountry: string = "";
  selectedCountry: Country = <Country>{};
  newCountry: Country = <Country>{};


  ngOnInit(): void {
    this.fetchCountries();
  }
  onCountryChanged(countryName: string): void {
    this.selectedCountry = this.countries.find(country => country.name === countryName) as Country
  }
  fetchCountries(): void {
    this.apiService.getAllCountries().subscribe((data) =>{
      this.countries=data
    })
  }
  updateCountry(): void{
    this.apiService.putCountry(this.selectedCountry);
    this.fetchCountries();
  }
  addCountry(): void {
    if(!StorageUtil.readCountries()?.find(c => c.name.toLowerCase() === this.newCountry.name.toLowerCase())){
      this.apiService.addCountry(this.newCountry).subscribe(() => {
        this.apiService.getAllCountries().subscribe((data) =>{
          this.countries=data
          StorageUtil.saveCountries(data);
          this.newCountry = <Country>{};
        })
      })
    }
    else{
      alert("Country already exists!")
    }
    
    
  }

}
