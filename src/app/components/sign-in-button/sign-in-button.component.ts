import { Component, OnInit } from '@angular/core';
import { KeycloakLoginOptions } from 'keycloak-js';
import keycloak from 'src/keycloak';

@Component({
  selector: 'app-sign-in-button',
  templateUrl: './sign-in-button.component.html',
  styleUrls: ['./sign-in-button.component.css']
})
export class SignInButtonComponent {

  get authenticated(): boolean {
    return Boolean(keycloak.authenticated);
  }

  handleLogin(): void {
    console.log('login');
    console.log(keycloak)
    keycloak.login(<KeycloakLoginOptions>{redirectUri: "https://boxinator-case.herokuapp.com/afterlogin"});
  }

  handleLogout(): void {
    sessionStorage.clear();
    keycloak.logout();
  }
  isAdmin(): boolean {
    if(!keycloak.authServerUrl){
      return false;
    }
    return Boolean(keycloak.hasRealmRole('ADMIN'));
  }

  constructor() {
    if(keycloak.tokenParsed){
      console.log(keycloak.tokenParsed['roles'])
    }
   }



}
