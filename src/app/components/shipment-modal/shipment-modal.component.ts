import { Component, Input, OnInit } from '@angular/core';
import { EditShipment, Shipment, Status } from 'src/app/models/shipment.model';
import { ApiService } from 'src/app/services/api.service';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-shipment-modal',
  templateUrl: './shipment-modal.component.html',
  styleUrls: ['./shipment-modal.component.css']
})
export class ShipmentModalComponent implements OnInit {

  @Input() shipment?: Shipment;
  tmpStatus?: Status;
  statuses?: Status[];
  userService: any;
  tmpShipment?: EditShipment;
  tmpStatusIds: number[] = [];
  tmpPackageIds: number[] = [];

  constructor(public modalService: ModalService, private apiService: ApiService) { 
    this.statuses = [
      {id: 1, name: "CREATED"}, 
      {id: 2, name: "RECIEVED"}, 
      {id: 3, name: "INTRANSIT"}, 
      {id: 4, name: "COMPLETED"}, 
      {id: 5, name: "CANCELLED"}];
  }

  ngOnInit(): void {
  }

  changeStatus() {
    console.log(this.statuses);
  }

  updateStatus() {
    this.modalService.shipment.status.forEach(s => {
      this.tmpStatusIds?.push(s.id as number);
    });

    this.tmpStatusIds.push(this.tmpStatus?.id as number);

    this.tmpShipment = {
      id: this.modalService.shipment.id,
      userId: this.modalService.shipment.userId,
      receiverName: this.modalService.shipment.receiverName,
      totalCost: this.modalService.shipment.totalCost,
      totalWeight: this.modalService.shipment.totalWeight,
      countryId: this.modalService.shipment.countryId,
      packages: this.modalService.shipment.packages,
      statusIds: this.tmpStatusIds
    };

    this.updateShipment();
    this.modalService.shipment.status.push(this.tmpStatus as Status);
  }

  updateShipment() : void{
    this.apiService.putShipment(this.tmpShipment as EditShipment);
  }
}
