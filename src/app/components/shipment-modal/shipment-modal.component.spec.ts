import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShipmentModalComponent } from './shipment-modal.component';

describe('ShipmentModalComponent', () => {
  let component: ShipmentModalComponent;
  let fixture: ComponentFixture<ShipmentModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShipmentModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShipmentModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
