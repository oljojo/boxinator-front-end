import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ModalService } from 'src/app/services/modal.service';
import {NgForm} from '@angular/forms'
import { Country } from 'src/app/models/country.model';
import keycloak from 'src/keycloak';
import { StorageUtil } from 'src/app/utils/storage.util';
import { Status, Tier, User, EditShipment, EditPackages } from 'src/app/models/shipment.model';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {


  constructor(public modalService: ModalService, private apiService: ApiService){}
  totalWeight = 0;
  totalCost = 0;
  packages: EditPackages[] = [];
  countries: Country[] = [];
  statuses: Status[] = [{name: "CREATED", id: 1}, {name: "RECIEVED", id: 2}, {name: "INTRANSIT", id: 3}, {name: "COMPLETED", id: 4}, {name: "CANCELLED", id: 5}];
  tiers: Tier[] = [{name: "Basic", id: 1, minWeight: 0, maxWeight: 1}, {name: "Humble", id: 2, minWeight: 1, maxWeight: 2}, {name: "Deluxe", id: 3, minWeight: 2, maxWeight: 5}, {name: "Premium", id: 4, minWeight: 5, maxWeight: 8}];
  currCountry: Country = <Country>{};
  colors: string[] = ["Red", "Blue", "Green", "Yellow", "Purple", "Black", "Brown", "White"];

  user: User | undefined = StorageUtil.readUser();

  get authenticated(): boolean {
    return Boolean(keycloak.authenticated);
  }

  ngOnInit(): void {

    this.fetchCountries();
  }

  fetchCountries(): void {
    this.apiService.getAllCountries().subscribe((data) =>{
      this.countries = data;
      StorageUtil.saveCountries(data);

    }
    );
  }
  // fetchUserId(): number{
  //   return getUserByKCId();
  // }

  calculatePrice(){
    //get country rate
    //calculate price
    this.totalCost = 0;
    this.packages.forEach(p =>{
      //this.totalCost += p.weight! * p.tierId! * this.currCountry.rate;
      if(p.weight != undefined && p.weight > 0){
        this.totalCost += p.weight! * 4;
      }
    })
  }
  calculateWeight(){
    //add all package weights
    this.totalWeight = 0;
    this.packages.forEach(p =>{
      if(p.weight != undefined && p.weight > 0){
        this.totalWeight += p.weight!;
      }
    })
  }

  sendShipment(shipment: NgForm): void {
    console.log(shipment.value)
    let newShipment: EditShipment = <EditShipment>{};
    newShipment.userId = this.user!.id as number;
    newShipment.countryId = Number(shipment.value.country);
    newShipment.receiverName = shipment.value.name;
    newShipment.totalCost = this.totalCost;
    newShipment.totalWeight = this.totalWeight;
    newShipment.statusIds = [1];
    newShipment.packages = this.packages;
    console.log(newShipment);
    this.apiService.addShipment(newShipment).subscribe(() =>
      this.apiService.getShipmentsByUser(this.user!.id as number).subscribe(response => {
        StorageUtil.saveShipments(response).then(() => {
          this.modalService.showModal = false;
          this.modalService.shipmentEmitter();
        });
      })
    )
    
    
    
  }

  changeWeight(pack: EditPackages, weight: string){
    pack.weight = Number(weight);
  }
  changeColor(pack: EditPackages, color: string){
    pack.color = color;
  }
  addPackageInput():void {
    let pack: EditPackages = <EditPackages>{};
    this.packages.push(pack);
  }
  setTier(pack: EditPackages):void {
    if (pack.weight! <= this.tiers[0].maxWeight){
      pack.tierId = this.tiers[0].id
    }
    else if(pack.weight! <= this.tiers[1].maxWeight && pack.weight! > this.tiers[1].minWeight){
      pack.tierId = this.tiers[1].id
    }
    else if(pack.weight! <= this.tiers[2].maxWeight && pack.weight! > this.tiers[2].minWeight){
      pack.tierId = this.tiers[2].id
    }
    else if(pack.weight! <= this.tiers[3].maxWeight && pack.weight! > this.tiers[3].minWeight){
      pack.tierId = this.tiers[3].id
    }
  }
  removePackageInput(pack: EditPackages):void {
    const i = this.packages.indexOf(pack, 0);
    if (i > -1) {
      this.packages.splice(i, 1);
    }
  }


  // public sendEmail(e: Event) {
  //   let x : any = document.getElementById("x");
  //   let xc : any = document.getElementById("xc")

  //   e.preventDefault();
  //   emailjs.sendForm(environment.service_id,environment.template_id , e.target as HTMLFormElement, environment.public_key)
  //     .then((result: EmailJSResponseStatus) => {
  //       xc.style.display = "block";
  //       x.style.display = "block";
  //       console.log(result.text);
  //     }, (error) => {
  //       console.log(error.text);
  //     });
  // }




}
