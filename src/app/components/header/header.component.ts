import { Component, OnInit } from '@angular/core';
import keycloak from 'src/keycloak';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  get authenticated(): boolean {
    return Boolean(keycloak.authenticated);
  }
  
  constructor() { }

  ngOnInit(): void {
  }

}
