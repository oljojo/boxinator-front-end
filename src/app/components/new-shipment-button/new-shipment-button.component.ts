import { Component, OnInit } from '@angular/core';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-new-shipment-button',
  templateUrl: './new-shipment-button.component.html',
  styleUrls: ['./new-shipment-button.component.css']
})
export class NewShipmentButtonComponent implements OnInit {

  constructor(public modalService: ModalService) { }

  ngOnInit(): void {
  }

}
