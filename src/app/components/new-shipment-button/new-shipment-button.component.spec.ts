import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewShipmentButtonComponent } from './new-shipment-button.component';

describe('NewShipmentButtonComponent', () => {
  let component: NewShipmentButtonComponent;
  let fixture: ComponentFixture<NewShipmentButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewShipmentButtonComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NewShipmentButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
