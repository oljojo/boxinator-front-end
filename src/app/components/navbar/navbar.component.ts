import { Component, OnInit } from '@angular/core';
import keycloak from 'src/keycloak';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  get role(): boolean {
    return Boolean(keycloak.hasRealmRole("admin"));
  }

  constructor() { }

  ngOnInit(): void {
  }

}
