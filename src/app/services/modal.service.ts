import { EventEmitter, Injectable, Output } from '@angular/core';
import { Shipment } from '../models/shipment.model';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  @Output() shipmentEmit = new EventEmitter<void>();

  shipmentEmitter() {
    this.shipmentEmit.emit();
  }


  showModal = false;
  showShipmentModal = false;


  shipment: Shipment = <Shipment>{};
  package: any;

  constructor() { }
}
