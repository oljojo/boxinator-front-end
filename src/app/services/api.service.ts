import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Country, EditShipment, Shipment, User} from '../models/shipment.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private _http: HttpClient) { }

  public addUser(user: User): void {
    const body = user;
    console.log(body)
    let url = "https://localhost:7041/api/v1/users/Register"
    this._http.post<User>(url, body).subscribe();
  }

  public putUser(user: User): void {
    const body = user;
    console.log(body)
    let url = `https://api-boxinator.herokuapp.com/api/v1/users/${user.id}`;
    this._http.put<User>(url, body).subscribe();
  }

  public getAllUsers(): Observable<User[]> {
    const url = `https://api-boxinator.herokuapp.com/api/v1/users/getAllUsers`;
    return this._http.get<User[]>(url);
  }
  
  public getUser(userId: number): Observable<User> {
    const url = `https://api-boxinator.herokuapp.com/api/v1/users/getUserbyId?id=${userId}`;
    return this._http.get<User>(url);
  }

  public getUserByKC(keycloakId: string): Observable<User> {
    const url = `https://api-boxinator.herokuapp.com/api/v1/users/getUserbykeyCloakId?keyloakId=${keycloakId}`;
    return this._http.get<User>(url);
  }
  public registerUser(user: User) {
    const body = user;
    const url = `https://api-boxinator.herokuapp.com/api/v1/users/Register`;
    return this._http.post(url, body);
  }

  public addCountry(country: Country): Observable<void> {
    console.log(country)
    const body =  country;
    console.log(body.name)
    let url="https://api-boxinator.herokuapp.com/api/Countries"
    return this._http.post<void>(url, body);
  }
  public putCountry(country: Country): void {
    const body =  country;
    console.log(body)
    let url=`https://api-boxinator.herokuapp.com/api/Countries/${country.id}`
    this._http.put<Country>(url, body).subscribe();
  }

  public getCountryById(countryId: number): Observable<Country> {
    const url = `https://api-boxinator.herokuapp.com/api/Countries/${countryId}`;
    return this._http.get<Country>(url);
  }
  public getAllCountries(): Observable<Country[]> {
    const url = `https://api-boxinator.herokuapp.com/api/Countries/`;
    return this._http.get<Country[]>(url);
  }

  public getShipments() {
    let url="https://api-boxinator.herokuapp.com/api/Shipments"
    return this._http.get<Shipment[]>(url)
  }

  public addShipment(shipment: EditShipment) {
    const body = JSON.stringify(shipment);
    const header = {"Content-Type": "application/json"}
    console.log(body)
    let url="https://api-boxinator.herokuapp.com/api/Shipments"
    return this._http.post(url, body, {headers: header});
  }

  public getTiers() {
    let url="https://api-boxinator.herokuapp.com/api/Shipments"
    return this._http.get<Shipment[]>(url)
  }

  public getShipmentsByUser(userId: number): Observable<Shipment[]> {
    const url = `https://api-boxinator.herokuapp.com/api/Shipments/user/${userId}`;
    return this._http.get<Shipment[]>(url);
  }


  public putShipment(shipment: EditShipment): void {
    const url = `https://api-boxinator.herokuapp.com/api/Shipments/${shipment.id}`;
    const body = shipment;
    this._http.put<EditShipment>(url, body).subscribe();
  }
 

}
