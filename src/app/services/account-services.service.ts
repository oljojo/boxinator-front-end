import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user.model';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AccountServicesService {

  //url : string = "https://localhost:7041/api/v1/users/getallUsers"
  // 
  constructor(private http: HttpClient) { }

  getUsers() {
    let url = "https://api-boxinator.herokuapp.com/api/v1/users/getallUsers"
    return this.http.get<User[]>(url)
  }

  addUser(user: User): void {
    const body = user;
    console.log(body)
    let url = "https://api-boxinator.herokuapp.com/api/v1/users/Register"
    this.http.post<User>(url, body).subscribe();
  }


  getUserbyId(keycloakId: string): Observable<User> {
    const url = `https://api-boxinator.herokuapp.com/api/v1/users/getUserbykeyCloakId?keyloakId=${keycloakId}`;
    return this.http.get<User>(url);
  }


  putUser(user: User): void {
    const body = user;
    console.log(body)
    let url = `https://api-boxinator.herokuapp.com/api/v1/users/${user.id}`;
    this.http.put<User>(url, body).subscribe();
  }

}
