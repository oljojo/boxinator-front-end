import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Country } from '../models/country.model';
import { Shipment } from '../models/modal.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class CountryServiceService {


  constructor(private readonly http: HttpClient) { }


  getCountries(){
    let url="https://api-boxinator.herokuapp.com/api/Countries"
    return this.http.get<Country[]>(url);
  }

  addCountry(country: Country): void {
    console.log(country)
    const body =  country;
    console.log(body.name)
    let url="https://api-boxinator.herokuapp.com/api/Countries"
    this.http.post<Country>(url, body).subscribe();
  }
  putCountry(country: Country): void {
    const body =  country;
    console.log(body)
    let url=`https://api-boxinator.herokuapp.com/api/Countries/${country.id}`
    this.http.put<Country>(url, body).subscribe();
  }

  getShipments() {
    let url="https://api-boxinator.herokuapp.com/api/Shipments"
    return this.http.get<Shipment[]>(url)
  }

  addShipment(shipment: Shipment): void {
    const body = JSON.stringify(shipment);
    const header = {"Content-Type": "application/json"}
    console.log(body)
    let url="https://api-boxinator.herokuapp.com/api/Shipments"
    this.http.post<Shipment>(url, body, {headers: header}).subscribe();
  }
  putShipment(shipment: Shipment): void {
    const body =  shipment;
    console.log(body)
    let url=`https://api-boxinator.herokuapp.com/api/Shipments/${shipment.id}`
    this.http.put<Shipment>(url, body).subscribe();
  }
  getTiers() {
    let url="https://api-boxinator.herokuapp.com/api/Shipments"
    return this.http.get<Shipment[]>(url)
  }

}
