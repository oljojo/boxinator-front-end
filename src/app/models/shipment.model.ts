export interface Shipment {
    id: number,
    userId: number,
    receiverName: string,
    countryId: number,
    status: Status[],
    totalWeight: number,
    totalCost: number,
    packages: Packages[],
    country?: Country,
    user?: User
}

export interface EditShipment {
    id: number,
    userId: number,
    receiverName: string,
    countryId: number,
    totalWeight: number,
    totalCost: number,
    statusIds: number[],
    packages: EditPackages[]
}

export interface Status {
    id: number,
    name: string
}

export interface Packages {
    id: number,
    shipmentId: number,
    weight: number,
    color: string,
    tier?: Tier
}

export interface EditPackages {
    id: number,
    shipmentId: number,
    weight: number,
    color: string,
    tierId?: number
}

export interface Tier {
    id: number,
    name: string
    maxWeight: number
    minWeight: number
}

export interface Country {
    id: number,
    name: string,
    rate: number
}

export interface User {


    id?: number | null | undefined,
    keyCloakId?: string | null,
    firstName?: string | null,
    lastName?: string | null,
    email?: string | null,
    dateOfBirth?: string | null,
    country?: string | null,
    zipCode?: string | null,
    phoneNumber?: string | null,
    shipmentIds?: number[] | null
}