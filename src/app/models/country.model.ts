export interface Country
{
  id: number
  name: string
  rate: number
}

