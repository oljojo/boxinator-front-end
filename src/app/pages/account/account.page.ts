import { Component, OnInit } from '@angular/core';
import * as moment from 'moment'
import keycloak from 'src/keycloak';
import { ApiService } from 'src/app/services/api.service';
import { User } from 'src/app/models/shipment.model';


@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.css']
})

export class AccountPage implements OnInit {

  constructor(private apiService: ApiService ) { }
  users = <User[]>[];
  selectedUser: User = <User>{};
  user: User = <User>{};
  updatedUser: User = <User>{};

  ngOnInit(): void {
    this.fetchUsers();
    this.getspecifUsers();
  }
  fetchUsers(): void {
    this.apiService.getAllUsers().subscribe((data) =>{
      this.users=data
    })
  }

  getspecifUsers(): void {
    this.apiService.getUserByKC(keycloak.tokenParsed?.sub as string).subscribe((data) =>{
      console.log(data.dateOfBirth)
      let date = data.dateOfBirth
      let ss = new Date(date!);
      let dateString = moment(ss).format('YYYY-MM-DD')
      data.dateOfBirth = dateString
      this.user=data

    })

  }


}


