import { Component, Input, OnInit } from '@angular/core';
import { Shipment, User } from 'src/app/models/shipment.model';
import { ApiService } from 'src/app/services/api.service';
import { ModalService } from 'src/app/services/modal.service';
import { StorageUtil } from 'src/app/utils/storage.util';
import keycloak from 'src/keycloak';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.css']
})
export class HomePage implements OnInit {

  @Input() shipment: Shipment[] = [];

  shipments: Shipment[] | undefined = <Shipment[]>[];

  user: User | undefined;


  get authenticated(): boolean {
    return Boolean(keycloak.authenticated);
  }
  get role(): boolean {
    return Boolean(keycloak.hasRealmRole("admin"));
  }

  constructor(private apiService: ApiService, private modalService: ModalService) { 
    this.user = StorageUtil.readUser();
  }

  

  ngOnInit() {
    if(this.role){
      //get all shipments
      this.getShipmentsForAdmin();
    }
    else if (this.authenticated){
      this.getShipmentsForUser();
      console.log(this.shipments);
      
      this.modalService.shipmentEmit.subscribe(() => {
        console.log("In emitter")
        this.getShipmentsForUser();
        console.log(this.shipments)
      })
    }
  }

  getShipmentsForUser(){
    this.apiService.getShipmentsByUser(this.user!.id!).subscribe(shipments => {
      shipments.map(s => {
        if(StorageUtil.readCountries()){
          s.country = StorageUtil.readCountries()?.find(c => s.countryId === c.id);
        }
        else{
          this.apiService.getAllCountries().subscribe(countries => {
            StorageUtil.saveCountries(countries);
            countries.map(c => {
              if(c.id === s.countryId){
                s.country = c;
              }
            })
          })
        }
        s.user = this.user;
      })
      this.shipments = shipments;
    })
  }
  getShipmentsForAdmin(){
    this.apiService.getShipments().subscribe(shipments => {
      shipments.map(s => {
        if(StorageUtil.readCountries()){
          s.country = StorageUtil.readCountries()?.find(c => s.countryId === c.id);
        }
        else{
          this.apiService.getAllCountries().subscribe(countries => {
            StorageUtil.saveCountries(countries);
            countries.map(c => {
              if(c.id === s.countryId){
                s.country = c;
              }
            })
          })
        }
        this.apiService.getUser(s.userId).subscribe(u => {
          s.user = u;
        })
      })
      this.shipments = shipments;
    })
  }

}
