import { Component, Input, OnInit } from '@angular/core';
import { Shipment, User } from 'src/app/models/shipment.model';
import { ApiService } from 'src/app/services/api.service';
import { StorageUtil } from 'src/app/utils/storage.util';
import keycloak from 'src/keycloak';

@Component({
  selector: 'app-shipments',
  templateUrl: './shipments.page.html',
  styleUrls: ['./shipments.page.css']
})
export class ShipmentsPage implements OnInit {

  @Input() shipment: Shipment[] = [];

  shipments: Shipment[] | undefined = <Shipment[]>[];


  user: User | undefined = StorageUtil.readUser();

  
  get role(): boolean {
    return Boolean(keycloak.hasRealmRole("admin"));
  }

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    if(this.role){
      //get all shipments
      this.getShipmentsForAdmin();
    }
    else{
      this.getShipmentsForUser();
      console.log(this.shipments);
    }
  }

  getShipmentsForUser(){
    this.apiService.getShipmentsByUser(this.user!.id!).subscribe(shipments => {
      shipments.map(s => {
        if(StorageUtil.readCountries()){
          s.country = StorageUtil.readCountries()?.find(c => s.countryId === c.id);
        }
        else{
          this.apiService.getAllCountries().subscribe(countries => {
            StorageUtil.saveCountries(countries);
            countries.map(c => {
              if(c.id === s.countryId){
                s.country = c;
              }
            })
          })
        }
        s.user = this.user;
      })
      this.shipments = shipments;
    })
  }
  getShipmentsForAdmin(){
    this.apiService.getShipments().subscribe(shipments => {
      shipments.map(s => {
        if(StorageUtil.readCountries()){
          s.country = StorageUtil.readCountries()?.find(c => s.countryId === c.id);
        }
        else{
          this.apiService.getAllCountries().subscribe(countries => {
            StorageUtil.saveCountries(countries);
            countries.map(c => {
              if(c.id === s.countryId){
                s.country = c;
              }
            })
          })
        }
        this.apiService.getUser(s.userId).subscribe(u => {
          s.user = u;
        })
      })
      this.shipments = shipments;
    })
  }
}
