import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/shipment.model';
import { ApiService } from 'src/app/services/api.service';
import { StorageUtil } from 'src/app/utils/storage.util';
import keycloak from 'src/keycloak';

@Component({
  selector: 'app-after-login',
  templateUrl: './after-login.page.html',
  styleUrls: ['./after-login.page.css']
})
export class AfterLoginPage implements OnInit{


  constructor(private apiService: ApiService, private router: Router) { }
  ngOnInit() {
    //try to fetch a user, using sub
    this.apiService.getUserByKC(keycloak.tokenParsed?.sub as string)
    .subscribe({
      next: (user) => {
        StorageUtil.saveUser(user).then(() => {
          console.log("Saved user");
          return this.router.navigateByUrl("/");
        });
      },
      error: () => {
        this.apiService.registerUser(this.parsedUser()).subscribe(() => {
          this.apiService.getUserByKC(keycloak.tokenParsed?.sub as string).subscribe(user => {
            StorageUtil.saveUser(user).then(() => {
              console.log("Saved user");
              return this.router.navigateByUrl("/");
            });
          })
        })
        
      }
      });
  }
  parsedUser(): User {
    let user: User = <User>{};
    user.firstName = keycloak.tokenParsed?.given_name as string;
    user.lastName = keycloak.tokenParsed?.family_name as string;
    user.country = keycloak.tokenParsed?.country;
    user.dateOfBirth = keycloak.tokenParsed?.dob;
    user.email = keycloak.tokenParsed?.email;
    user.keyCloakId = keycloak.tokenParsed?.sub;
    user.phoneNumber = keycloak.tokenParsed?.mobile;
    user.shipmentIds = null;
    user.zipCode = keycloak.tokenParsed?.zip;
    return user
  }

}
