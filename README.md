# Boxinator-Front-End
The project is the front-end for the whole Boxinator projcet. This front-end application is connected to [Keycloak](https://www.keycloak.org/) so users can login and register to the application. In the application a user can create, send and view shipment history, and admins can view the shipment history for all users. If a users is not yet registered, then it is only possible to register or send a shipment.

## Table of Contents
- [Clone](#Clone)
- [Install](#Install)
- [Start](#Start)
- [Usage](#Usage)
- [Api](#API)
- [Contributers](#Contributers)

## Clone
You can clone this repository for your own uses through the following comman in your terminal:
```
cd <your-chosen-folder>
git clone https://gitlab.com/oljojo/boxinator-front-end.git
```

## Install
After downloading the code, open the application through the following command in the terminal:
```
cd boxinator-front-end
code .
```

After that you need to install all the npm dependeices with the following command in your terminal:
```
npm install
```

## Start
Once the application is opened in your IDE, it can easely be run by entering the following command: `ng serve`. After that you only need to click on the url that is shown in the terminal.

## Usage
The application will start on the home page where you can choose to register a new accounnt, sign in or send a new shipment. If you do not have an account you will get an email sent to you with the shipment information and a register link provided in the mail. 

If you have an account and sign in, you will be able to create a new shipment and view your own shipment history and also be able to change your account information.

If you have an admin account you can see the shipment information for all users, change the status of a shipment and change or add a country.

## API
You need to update the environment.prod.ts file with the following paramters in order to make the EmailJs work:
```
production: true,
service_id: '',
template_id: '',
public_key: ''
```
More information can be found in the EmailJs documentation: https://www.emailjs.com/docs/

## Known bugs/Future development
* The functionality for guest-user is not yet created
* Shipments cannot be sorted by status 
* Shipments cannot be sorted by date

## Contributers
- Jeremy - @Jerry585
- Olof - @oljojo
- Usko - @usk1129
